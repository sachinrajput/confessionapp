<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->image_upload_path = 'assets/img/users/images/';
	}
	
	public function login(){
	
		var_dump($_POST);
		
	}
	
	public function get_school_list($userid) {
		if ($this->ion_auth->logged_in() === true) {
		//$this->db->where('school_name', $school_name); 
		$this->db->where('userid', $userid); 
		
		$school_details = $this->db->get('user_schools');
	
		print '<li>
			<a href="#">Add School</a>
			<ul class="dl-submenu">
				<li><div class="well">
				    <div class="typeahead-wrapper1" style="text-align: center;">
				    	<form method="post" action="'.base_url().'schools/add_school" id="add_school" >
				      <input class="schools" name="school_name" type="text" placeholder="Type the School Name" value="">
				      <input type="hidden" name="userid" value="'.$userid.'" />
				      <button class="btn btn-success" style="margin-top: 7px;" type="submit" title="Add School">Add</button>
				     	</form>
				    </div>
				</div></li>
				
			</ul>
		</li>';
		
		if ($school_details->num_rows() > 0){
			foreach ($school_details->result() as $row)
			{
				$this->db->where('school_name', $row->school_name); 
				
				$school_details1 = $this->db->get('schools');
				
				foreach ($school_details1->result() as $row1)
				{
					$school_link = base_url().'school/'.$row1->url_code;
					print ' <li>
								<a href="'.$school_link.'">'.$row1->school_name.'</a>
							</li>';
				}
				
			}
		}
		
		} else {
			//redirect them to the login page
			redirect('login', 'refresh');
		}
	}
	
	public function get_image($userid,$mode) {
		$im = $this->user_functions->image_html($userid,$mode);
		return $im;
	}
	
	public function stream_image($userid,$mode,$stream=null) {
		if($stream == NULL)
			$im = $this->user_functions->image_html($userid,$mode);
		else 
			$im = $this->user_functions->image_html($userid,$mode,$stream);
		echo $im;
	}
	
	public function changecolor() {
		if(isset($_POST)){
			$mode = $this->input->post('option');
			$userid = $this->input->post('userid');
			$color = $this->input->post('color');
			
			// Insert info into database table!
			$this->db->where('userid', $userid); 
			$this->db->where('option', $mode); 
			
			$query = $this->db->get('userimage');
			
			if ($query->num_rows() > 0)
			{
			   foreach ($query->result() as $row)
			   {
			      $data = array(
			         'option' => $mode,
			         'color' => $color,
			         'choose' => 2,
			         'datetime' => date('Y-m-d H:i:s')
			      );
			      
			      $this->db->where('userid', $row->userid);
			      $this->db->where('option', $mode);
			      $this->db->update('userimage', $data); 
			     
			   }
			  
			}
			else {
				$data = array(
				   'userid' => $userid ,
				   'imagename' => '' ,
				   'thumbname' => '',
				   'imgpath'=> '',
				   'option' => $mode,
				   'choose' => 2,
				   'color' => $color,
				   'datetime' => date('Y-m-d H:i:s')
				);
				
				$this->db->insert('userimage', $data);
				
			}
			
			echo 'Color updated successfully.<br/>This window will close in 3 seconds.<br />';
			
		}
	}
	
	public function uploadphoto() {
		if(isset($_POST))
		{
			//create unique folder for each user and lets delete existing photos if updated 
			$mode = $this->input->post('option');
			$userid = $this->input->post('userid');
			$maxsize    = 2097152;
			if(($_FILES['ImageFile']['size'] >= $maxsize) || ($_FILES["ImageFile"]["size"] == 0)) {
			        die('ErrorSize');
			}
			
			if($mode == 'profile'){
			
				if (!file_exists('assets/img/users/images/'.$userid.'/profile')) {
				    mkdir('assets/img/users/images/'.$userid.'/profile', 0777, true);
				    $this->image_upload_path = 'assets/img/users/images/'.$userid.'/profile/';
				} else {
					$this->image_upload_path = 'assets/img/users/images/'.$userid.'/profile/';
					
					$files = glob($this->image_upload_path.'*'); // get all file names
					foreach($files as $file){ // iterate files
					  if(is_file($file))
					    unlink($file); // delete file
					}
				}
				
			} else {
				
				if (!file_exists('assets/img/users/images/'.$userid.'/background')) {
				    mkdir('assets/img/users/images/'.$userid.'/background', 0777, true);
				    $this->image_upload_path = 'assets/img/users/images/'.$userid.'/background/';
				} else {
					$this->image_upload_path = 'assets/img/users/images/'.$userid.'/background/';
					
					$files = glob($this->image_upload_path.'*'); // get all file names
					foreach($files as $file){ // iterate files
					  if(is_file($file))
					    unlink($file); // delete file
					}
				}
				
			}
			//var_dump($_POST);
		     //Some Settings
		    $ThumbSquareSize        = 200; //Thumbnail will be 200x200
		    $BigImageMaxSize        = 500; //Image Maximum height or width
		    $ThumbPrefix            = "thumb_"; //Normal thumb Prefix
		    $DestinationDirectory           =  $this->image_upload_path;//Upload Directory ends with / (slash)
		    $Quality                = 90;
		
		    // check $_FILES['ImageFile'] array is not empty
		    // "is_uploaded_file" Tells whether the file was uploaded via HTTP POST
		    if(!isset($_FILES['ImageFile']) || !is_uploaded_file($_FILES['ImageFile']['tmp_name']))
		    {
		    		//return 0;
		            die('Error'); // output error when above checks fail.
		    }
		
		    // Random number for both file, will be added after image name
		    $RandomNumber   = rand(0, 9999999999);
		
		    // Elements (values) of $_FILES['ImageFile'] array
		    //let's access these values by using their index position
		    
		    $ImageName      = str_replace(' ','-',strtolower($_FILES['ImageFile']['name']));
		    $ImageSize      = $_FILES['ImageFile']['size']; // Obtain original image size
		    $TempSrc        = $_FILES['ImageFile']['tmp_name']; // Tmp name of image file stored in PHP tmp folder
		    $ImageType      = $_FILES['ImageFile']['type']; //Obtain file type, returns "image/png", image/jpeg, text/plain etc.
		
		
			
			    
		    //Let's use $ImageType variable to check wheather uploaded file is supported.
		    //We use PHP SWITCH statement to check valid image format, PHP SWITCH is similar to IF/ELSE statements
		    //suitable if we want to compare the a variable with many different values
		    switch(strtolower($ImageType))
		    {
		        case 'image/png':
		            $CreatedImage =  imagecreatefrompng($_FILES['ImageFile']['tmp_name']);
		            break;
		        case 'image/gif':
		            $CreatedImage =  imagecreatefromgif($_FILES['ImageFile']['tmp_name']);
		            break;
		        case 'image/jpeg':
		        case 'image/pjpeg':
		            $CreatedImage = imagecreatefromjpeg($_FILES['ImageFile']['tmp_name']);
		            break;
		        default:
		            die('Unsupported File!'); //output error and exit
		    }
		
		    //PHP getimagesize() function returns height-width from image file stored in PHP tmp folder.
		    //Let's get first two values from image, width and height. list assign values to $CurWidth,$CurHeight
		    list($CurWidth,$CurHeight)=getimagesize($TempSrc);
		    //Get file extension from Image name, this will be re-added after random name
		    $ImageExt = substr($ImageName, strrpos($ImageName, '.'));
		    $ImageExt = str_replace('.','',$ImageExt);
		
		    //remove extension from filename
		    $ImageName      = preg_replace("/\.[^.\s]{3,4}$/", "", $ImageName);
		
		    //Construct a new image name (with random number added) for our new image.
		    $NewImageName = $ImageName.'-'.$RandomNumber.'.'.$ImageExt;
		    //set the Destination Image
		    $thumb_DestRandImageName    = $DestinationDirectory.$ThumbPrefix.$NewImageName; //Thumb name
		    $DestRandImageName          = $DestinationDirectory.$NewImageName; //Name for Big Image
		
			//echo($DestRandImageName);
			//die();
		    //Resize image to our Specified Size by calling resizeImage function.
		    if($this->resizeImage($CurWidth,$CurHeight,$BigImageMaxSize,$DestRandImageName,$CreatedImage,$Quality,$ImageType))
		    {
		        //Create a square Thumbnail right after, this time we are using cropImage() function
		        if(!$this->cropImage($CurWidth,$CurHeight,$ThumbSquareSize,$thumb_DestRandImageName,$CreatedImage,$Quality,$ImageType))
		            {
		                echo 'Error Creating thumbnail';
		            }
		        /*
		        At this point we have succesfully resized and created thumbnail image
		        We can render image to user's browser or store information in the database
		        For demo, we are going to output results on browser.
		        */
				
				
				// Insert info into database table!
				$this->db->where('userid', $userid); 
				$this->db->where('option', $mode); 
				
				$query = $this->db->get('userimage');
				
				if ($query->num_rows() > 0)
				{
				   foreach ($query->result() as $row)
				   {
				      $data = array(
				         'imagename' => $DestRandImageName ,
				         'thumbname' => $thumb_DestRandImageName,
				         'imgpath'=> $DestinationDirectory,
				         'option' => $mode,
				         'choose' => 1,
				         'datetime' => date('Y-m-d H:i:s')
				      );
				      
				      $this->db->where('userid', $row->userid);
				      $this->db->where('option', $mode);
				      $this->db->update('userimage', $data); 
				     
				   }
				   $mesage = 'Updated existing ';
				}
				else {
					$data = array(
					   'userid' => $userid ,
					   'imagename' => $DestRandImageName ,
					   'thumbname' => $thumb_DestRandImageName,
					   'imgpath'=> $DestinationDirectory,
					   'option' => $mode,
					   'choose' => 1,
					   'datetime' => date('Y-m-d H:i:s')
					);
					
					$this->db->insert('userimage', $data);
					$mesage = 'New ';
				}
				
				 
				
		        //Get New Image Size
		        list($ResizedWidth,$ResizedHeight)=getimagesize($DestRandImageName);
				echo $mesage.'photo uploaded successfully.<br/>This window will close in 3 seconds.<br />';
				//echo('Photo uploaded for user: ');
				//var_dump($this->input->post('username'));
		        echo '<table width="100%" border="0" cellpadding="4" cellspacing="0">';
		        echo '<tr>';
		        echo '<td align="center"><img src="../'.$this->image_upload_path.$ThumbPrefix.$NewImageName.'" alt="Thumbnail" height="'.$ThumbSquareSize.'" width="'.$ThumbSquareSize.'"></td>';
		        //echo '</tr><tr>';
		        //echo '<td align="center"><img src="../'.$this->image_upload_path.$NewImageName.'" alt="Resized Image" height="'.$ResizedHeight.'" width="'.$ResizedWidth.'"></td>';
		        echo '</tr>';
		        echo '</table>';
		
		        
		        
		
		    }else{
		        die('Resize Error'); //output error
		    }
		
		}
		
	}
	
	
	// This function will proportionally resize image
	public function resizeImage($CurWidth,$CurHeight,$MaxSize,$DestFolder,$SrcImage,$Quality,$ImageType)
	{
	    //Check Image size is not 0
	    if($CurWidth <= 0 || $CurHeight <= 0)
	    {
	        return false;
	    }
	
	    //Construct a proportional size of new image
	    $ImageScale         = min($MaxSize/$CurWidth, $MaxSize/$CurHeight);
	    $NewWidth           = ceil($ImageScale*$CurWidth);
	    $NewHeight          = ceil($ImageScale*$CurHeight);
	
	    if($CurWidth < $NewWidth || $CurHeight < $NewHeight)
	    {
	        $NewWidth = $CurWidth;
	        $NewHeight = $CurHeight;
	    }
	    $NewCanves  = imagecreatetruecolor($NewWidth, $NewHeight);
	    // Resize Image
	    if(imagecopyresampled($NewCanves, $SrcImage,0, 0, 0, 0, $NewWidth, $NewHeight, $CurWidth, $CurHeight))
	    {
	        switch(strtolower($ImageType))
	        {
	            case 'image/png':
	                imagepng($NewCanves,$DestFolder);
	                break;
	            case 'image/gif':
	                imagegif($NewCanves,$DestFolder);
	                break;
	            case 'image/jpeg':
	            case 'image/pjpeg':
	                imagejpeg($NewCanves,$DestFolder,$Quality);
	                break;
	            default:
	                return false;
	        }
	    //Destroy image, frees up memory
	    if(is_resource($NewCanves)) {imagedestroy($NewCanves);}
	    return true;
	    }
	
	}
	
	//This function corps image to create exact square images, no matter what its original size!
	public function cropImage($CurWidth,$CurHeight,$iSize,$DestFolder,$SrcImage,$Quality,$ImageType)
	{
	    //Check Image size is not 0
	    if($CurWidth <= 0 || $CurHeight <= 0)
	    {
	        return false;
	    }
	
	    //abeautifulsite.net has excellent article about "Cropping an Image to Make Square"
	    //http://www.abeautifulsite.net/blog/2009/08/cropping-an-image-to-make-square-thumbnails-in-php/
	    if($CurWidth>$CurHeight)
	    {
	        $y_offset = 0;
	        $x_offset = ($CurWidth - $CurHeight) / 2;
	        $square_size    = $CurWidth - ($x_offset * 2);
	    }else{
	        $x_offset = 0;
	        $y_offset = ($CurHeight - $CurWidth) / 2;
	        $square_size = $CurHeight - ($y_offset * 2);
	    }
	
	    $NewCanves  = imagecreatetruecolor($iSize, $iSize);
	    if(imagecopyresampled($NewCanves, $SrcImage,0, 0, $x_offset, $y_offset, $iSize, $iSize, $square_size, $square_size))
	    {
	        switch(strtolower($ImageType))
	        {
	            case 'image/png':
	                imagepng($NewCanves,$DestFolder);
	                break;
	            case 'image/gif':
	                imagegif($NewCanves,$DestFolder);
	                break;
	            case 'image/jpeg':
	            case 'image/pjpeg':
	                imagejpeg($NewCanves,$DestFolder,$Quality);
	                break;
	            default:
	                return false;
	        }
	    //Destroy image, frees up memory
	    if(is_resource($NewCanves)) {imagedestroy($NewCanves);}
	    return true;
	
	    }
	
	}

}