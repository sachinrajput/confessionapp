<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Share extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('schools_functions');
		$this->load->helper('form');
	}
	
	function index() {
		if ($this->ion_auth->logged_in()) {
			$this->data['message_element'] = 'share/index';
			$this->data['title'] = 'Search your School';
			
			$this->load->view('template',$this->data);
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	function share_post($school_name,$postid) {
		//echo $school_name;
		
		if ($this->ion_auth->logged_in()) {
		
			$this->db->where('url_code', $school_name); 
			
			$school_details = $this->db->get('schools');
			
			foreach ($school_details->result() as $row)
			{	
				$this->data['school_id'] = $row->id;
				$this->data['school_name'] = $row->school_name;
				$this->data['school_domain'] = $row->school_domain;
				$this->data['school_url'] = $row->url_code;
				$this->data['school_logo'] = $row->school_logo;
				$this->data['school_admin'] = $row->admin_id;
				$this->data['title'] = $this->data['school_name'];
				$schoolid = $this->data['school_id'];
			}
			
			$this->db->where('school_id', $schoolid); 
			$this->db->where('userpostid', $postid); 
			
			$post_details = $this->db->get('messages_list');
			$this->data['error'] = false;
			if ($post_details->num_rows() > 0){
				foreach ($post_details->result() as $row)
				{	
					$this->data['message'] = $row->message;
					$this->data['post_type'] = $row->post_type;
					$this->data['post_no'] = $row->post_no;
					$this->data['comment_count'] = $row->comment_count;
					$this->data['likes_count'] = $row->likes;
					$this->data['datetime'] = $row->datetime;
				}
			} else {
				$this->data['error'] = true;
			}
			
			$this->data['message_element'] = 'share/index';
			//$this->data['school_id'] = $school_name;
			$this->data['postid'] = $postid;
			$this->data['title'] = $this->data['title'].' - Post Shared';
			
			$this->load->view('template',$this->data);
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
}