<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->library('session');
		$this->load->library('form_validation');
		$this->load->library('schools_functions');
		$this->load->helper('form');
	}
	
	function index() {
		if ($this->ion_auth->logged_in()) {
			$this->data['message_element'] = 'schools/index';
			$this->data['title'] = 'Search your School';
			
			$this->load->view('template',$this->data);
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	
	function route_school($url_code){
		if ($this->ion_auth->logged_in() === true) {
			$this->db->where('url_code', $url_code); 
			
			$school_details = $this->db->get('schools');
			
			foreach ($school_details->result() as $row)
			{
				$this->data['school_id'] = $row->id;
				$this->data['school_name'] = $row->school_name;
				$this->data['school_domain'] = $row->school_domain;
				$this->data['school_url'] = $row->url_code;
				$this->data['school_logo'] = $row->school_logo;
				$this->data['school_admin'] = $row->admin_id;
				$this->data['title'] = $this->data['school_name'];
			}
			
			$this->data['message_element'] = 'schools/school_page';
			
			$this->load->view('template',$this->data);
		} else {
			//redirect them to the login page
			redirect('login', 'refresh');
		}
	}
	
	function school_by_url_code($url_code) {
		echo "School URL CODE : ". $url_code;
	}
	
	function redirect_to_school_page() {
		if ($this->ion_auth->logged_in() === true) {
			$school_name = $this->input->post('school_name');	
			$this->db->where('school_name', $school_name); 
			
			$school_details = $this->db->get('schools');
			
			foreach ($school_details->result() as $row)
			{
				$school_url = $row->url_code;
			}
			
			redirect('school/'.$school_url,'refresh');
		} else {
			//redirect them to the login page
			redirect('login', 'refresh');
		}
	}
	
	function add_school() {
		if ($this->ion_auth->logged_in() === true) {
			$school_name = $this->input->post('school_name');
			$userid = $this->input->post('userid');
			
			$this->db->where('school_name', $school_name); 
			$this->db->where('userid', $userid); 
			
			$school_details = $this->db->get('user_schools');
			
			if ($school_details->num_rows() > 0){
				//error already exists
				echo '0';
			} else {
				//add school now
				$data = array(
				   'userid' => $userid ,
				   'school_name' => $school_name
				);
				
				$this->db->insert('user_schools', $data);
				echo '1';
			}
			
			
			//redirect('school/'.$school_url,'refresh');
		} else {
			//redirect them to the login page
			redirect('login', 'refresh');
		}
		
	}
	
}