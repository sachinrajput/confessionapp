<?php

class Secure extends CI_Controller {
	
    public function index() {
        $this->load->helper('url');
        if ($this->ion_auth->logged_in() === true) {
        	
        	$this->data['message_element'] = 'secure/home.php';
        	
        	$this->load->view('template',$this->data);
        	
        } else {
        
        	$this->data['message_element'] = 'auth/login.php';
        	
        	$this->load->view('template',$this->data);
        }
    }

    public function login() {
        $this->load->helper('url');
        if ($this->ion_auth->logged_in() === true) {
        	
        	$this->data['message_element'] = 'secure/home.php';
        	
        	$this->load->view('template',$this->data);
        } else {
        	
        	$this->data['message_element'] = 'auth/login.php';
        	
        	$this->load->view('template',$this->data);
        }
    }

}

