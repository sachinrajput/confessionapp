<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Sachin Rajput
*
* Author: Sachin Rajput
*		  sachin.rajput@hotmail.com	*
*/

class Schools_functions 
{
	public function __construct(){
		$this->_obj =& get_instance();
	}
	
	public function get_school_details($name){
		$this->_obj->db->where('school_name', $name); 
		
		$school_details = $this->db->get('schools');
		
		return $school_details->result();
	}
	
	public function get_list(){
		$school_list = $this->_obj->db->get('schools');
		$i = 0;
		//build list 
		foreach ($school_list->result() as $row)
		{
			if($i == 0)
				$list = '"'.$row->school_name.'"';
			else
				$list .= ', "'.$row->school_name.'"';
			$i++;
		}
		return $list;
	}
	
	public function get_school_list($userid) {
		
		//$this->db->where('school_name', $school_name); 
		$this->_obj->db->where('userid', $userid); 
		
		$school_details = $this->_obj->db->get('user_schools');
	
		print '<li>
			<a href="#">Add School</a>
			<ul class="dl-submenu">
				<li><div class="well">
				    <div class="typeahead-wrapper1" style="text-align: center;">
				    	<form method="post" action="'.base_url().'schools/add_school" id="add_school" >
				      <input class="schools" name="school_name" type="text" placeholder="Type the School Name" value="">
				      <input type="hidden" name="userid" value="'.$userid.'" />
				      <button class="btn btn-success" style="margin-top: 7px;" type="submit" title="Add School">Add</button>
				     	</form>
				    </div>
				</div></li>
				
			</ul>
		</li>';
		
		if ($school_details->num_rows() > 0){
			foreach ($school_details->result() as $row)
			{
				$this->_obj->db->where('school_name', $row->school_name); 
				
				$school_details1 = $this->_obj->db->get('schools');
				
				foreach ($school_details1->result() as $row1)
				{
					$school_link = base_url().'school/'.$row1->url_code;
					print ' <li>
								<a href="'.$school_link.'">'.$row1->school_name.'</a>
							</li>';
				}
				
			}
		}
		

	}
	

}
?>
