<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Sachin Rajput
*
* Author: Sachin Rajput
*		  sachin.rajput@hotmail.com	*
*/

class User_functions 
{
	public function __construct(){
		$this->_obj =& get_instance();
		
	}
	
	public function get_user_img($userid,$mode){
		$this->_obj->db->where('userid', $userid); 
		$this->_obj->db->where('option', $mode);
		
		$user_details = $this->_obj->db->get('userimage');
		//$row = $query->row(); 
		if ($user_details->num_rows() > 0)
			return $user_details->row();
		else
			return 0; 
	}
	
	public function image_html($userid,$mode,$stream=null) {
		
		$user_img = $this->get_user_img($userid,$mode);
		if(isset($user_img->choose) == 1){
			if($user_img){
			$img_path = base_url($user_img->thumbname); 
				if($mode != 'profile'){
					$img_path = 'background-image:url('.$img_path.'); ';
				}
			}
			else { 
				if($mode == 'profile'){
					$img_path = base_url('assets/img/no-image.gif');				
				}
				else {
					$img_path = 'style="background-image:none;"';
				}
			}
			
			if($mode == 'profile'){
				$img_html = '<img data-src="holder.js/300x200" alt="300x200" style="width: 215px; height: 200px;" src="'.$img_path.'">';
			}
			else {
				$img_html = $img_path;
			}
			//echo $img_html;
			//die();
			if($stream) return $user_img->imagename;
			return $img_html;
		} else if(isset($user_img->choose) == 2){
			//color
			if($stream) return $user_img->color;
			return 'background-color:'.$user_img->color;
		}
	}
	
	

}
?>
