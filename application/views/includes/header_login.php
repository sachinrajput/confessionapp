<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Confession App</title>
  <meta name="description" content="manage your contacts with name, email and phone">
  <meta name="author" content="confession">
  <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <!-- Le styles -->
  <link href="<?=base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/toggle-switch.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/bootstrap-responsive.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/docs.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/landing.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/secure-home.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Le fav and touch icons -->
  <link href="<?=base_url('assets/ico/favicon.ico'); ?>" rel="shortcut icon">
</head>
<?php
	if($this->ion_auth->logged_in()){
		$userid = $this->session->userdata('user_id');
	}
	$page_name = $this->uri->segment(1);
?>
<body class="body-class">
<div class="navbar navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <a class="pull-left btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <a class="brand login_brand" href="#">logo</a>
      <div class="nav-collapse">
        <ul class="nav pull-left">
          <li <?php if($page_name == 'login') echo 'class="active"';?>><a href="<?php echo base_url("login");?>">Login</a></li>
          <li <?php if($page_name == 'register') echo 'class="active"';?>><a href="<?php echo base_url("register");?>">Register</a></li>
        </ul>     	
      </div><!-- /.nav-collapse -->
    </div><!-- /.container -->
  </div><!-- /navbar-inner -->
</div>
