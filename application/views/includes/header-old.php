<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container-fluid">
        	<div class="pull-left" style="margin-left: 10px;z-index: 100;">
        		<div id="dl-menu" class="dl-menuwrapper">
        			<button class="dl-trigger" title="Add Schools Shortcut">Open Menu</button>
        			<ul class="dl-menu">
        				<?= $this->schools_functions->get_school_list($userid) ?>
        			</ul>
        		</div>
        	</div>
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span> 
            </a>
            <a class="brand" href="#">Confession</a>
            <div class="nav-collapse collapse">
            	
                <p class="navbar-text pull-right">
                	<div class="btn-group pull-right">
                	  <a class="btn btn-primary" href="#"><i class="icon-user icon-white"></i> User</a>
                	  <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
                	  <ul class="dropdown-menu">
                	    <li><a href="<?php echo base_url("account/".$userid);?>"><i class="icon-pencil"></i> Edit</a></li>
                	    
                	    <li class="divider"></li>
                	    <li><a href="<?php echo site_url('auth/logout'); ?>">Logout</a></li>
                	  </ul>
                	</div>
                </p>
                <div class="pull-right" style="margin-right: 10px;">
                	<a id="background" href="#change_back"><button class="btn btn-inverse" type="button"><i class="icon-picture icon-white"></i> Change Background</button></a>
                </div>
                <ul class="nav">
                    <!-- <li <?php if($page_name == 'home') echo 'class="active"';?>><a href="<?php echo base_url("home");?>">Home</a></li> -->
                    <li <?php if($page_name == 'schools') echo 'class="active"';?>><a href="<?php echo base_url("schools");?>">Search Schools</a></li>
                    <li <?php if($page_name == 'account') echo 'class="active"';?>><a href="<?php echo base_url("account/".$userid);?>">Account</a></li>
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
    
    
    <div id="change_back" style="display:none">
    
    	<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	        <li class="active"><a href="#red" data-toggle="tab">Choose / Upload Picture</a></li>
	        <li><a href="#orange" data-toggle="tab">Choose a Color</a></li>        
    	</ul>
	    <div id="my-tab-content" class="tab-content">
	        <div class="tab-pane active" id="red">
	            <form action="<?php echo base_url("ajax/uploadphoto")?>" method="post" enctype="multipart/form-data" id="UploadForm1">
	            Upload Background Photo 
	            <table width="500" border="0">
	              <tr>
	                <td>File : </td>
	                <td><input id="Imagefile1" name="ImageFile" type="file" /></td>
	              </tr>
	              <tr>
	                <td><input type="submit"  id="SubmitButton1" value="Upload" />
	                <br />You can only upload files < 2 MB
	                </td>
	              </tr>
	              <tr>
	                <td>&nbsp;
	                	<input type="hidden" name="option" value="back" />
	                	<input type="hidden" id="userid" name="userid" value="<?php echo $userid; ?>"  />
	                </td>
	              </tr>
	            </table>
	            
	            </form>
	        </div>
	        <div class="tab-pane" id="orange">
	            <form action="<?php echo base_url("ajax/changecolor")?>" method="post" enctype="multipart/form-data" id="changecolor">
				 Background Color :
	            <table width="500" border="0">
	              <tr>
	                <input type="text" id="color" name="color" value="#123456" />
	                	    	</form>
	                 <div id="colorpicker"></div>
	                 <div id="colorsetis" style="width: 40px;height: 30px;">&nbsp;</div>
	                 <button id="setbackcolor">set</button>
	                 <input type="hidden" name="colorset" id="colorset" value="" />
	                <div id="progressbox1"><div id="progressbar1"></div ><div id="statustxt1">0%</div ></div>
	                <div id="output1"></div>
	              </tr>
	              <tr>
	                <td>&nbsp;
	                	<input type="hidden" name="option" value="back" />
	                	<input type="hidden" id="color_handle" name="color1" value="" />
	                	<input type="hidden" id="userid" name="userid" value="<?php echo $userid; ?>"  />
	                </td>
	              </tr>
	            </table>
	            
	            </form>
	        </div>
    	</div>
    </div>
</div>