<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $title ?></title>
  <meta name="description" content="manage your contacts with name, email and phone">
  <meta name="author" content="confession">
  <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <!-- Le styles -->
  <link href="<?=base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/toggle-switch.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/bootstrap-responsive.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/docs.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/landing.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/secure-home.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/component.css'); ?>" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/colortip-1.0-jquery.css'); ?>"/>
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/farbtastic.css'); ?>"/>
  <script src="<?=base_url('assets/js/modernizr.custom.js'); ?>"></script>
  
  <!-- <link href="<?=base_url('assets/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css"> -->
    <!-- Le fav and touch icons -->
  <link href="<?=base_url('assets/ico/favicon.ico'); ?>" rel="shortcut icon">
  <link rel="stylesheet" type="text/css" href="<?=base_url('assets/css/jquery.fancybox.css?v=2.1.5'); ?>" media="screen" />
</head>
<?php
	if($this->ion_auth->logged_in()){
		$userid = $this->session->userdata('user_id');
	}
	$page_name = $this->uri->segment(1);
	$back_path = $this->user_functions->image_html($userid,'back');
	
	//echo($back_path);
?>
<body  style=<?php echo $back_path ?> class="body-class">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=126608517483969";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<div class="navbar navbar-fixed-top">
	  <div class="navbar-inner">
	    <div class="container">
			<div class="pull-left" style="margin-left: 10px;z-index: 100;">
				<div id="dl-menu" class="dl-menuwrapper">
					<button class="dl-trigger" title="Add Schools Shortcut">Open Menu</button>
					<ul class="dl-menu">
						<?= $this->schools_functions->get_school_list($userid) ?>
					</ul>
				</div>
			</div>
			<a class="brand" href="#">logo</a>
	      <a class="pull-left btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </a>
	      
	      <div class="nav-collapse">
	        <ul class="nav pull-left">
	          <li <?php if($page_name == 'schools') echo 'class="active"';?>><a href="<?php echo base_url("schools");?>"><i class="icon-search"></i> Search Schools</a></li>
	          <li <?php if($page_name == 'account') echo 'class="active"';?>><a href="<?php echo base_url("account/".$userid);?>"><i class="icon-user"></i> Account</a></li>
	        </ul>
	        
	        <ul class="nav pull-right">
	          <li><a href="<?php echo base_url("account/".$userid);?>"><i class="icon-pencil"></i> Edit</a></li>
	          <li><a id="background" href="#change_back"><i class="icon-picture"></i> Change Background</a></li>
	          <li class="divider"></li>
	          <li><a href="<?php echo site_url('auth/logout'); ?>"><i class="icon-minus-sign"></i> Logout</a></li>
	        </ul>	       	
	      </div><!-- /.nav-collapse -->
	    </div><!-- /.container -->
	  </div><!-- /navbar-inner -->
	</div>
	
	<div id="change_back" style="display:none">
	
		<ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
	        <li class="active"><a href="#red" data-toggle="tab">Choose / Upload Picture</a></li>
	        <li><a href="#orange" data-toggle="tab">Choose a Color</a></li>        
		</ul>
	    <div id="my-tab-content" class="tab-content">
	        <div class="tab-pane active" id="red">
	            <form action="<?php echo base_url("ajax/uploadphoto")?>" method="post" enctype="multipart/form-data" id="UploadForm1">
	            Upload Background Photo 
	            <table width="500" border="0">
	              <tr>
	                <td>File : </td>
	                <td><input id="Imagefile1" name="ImageFile" type="file" /></td>
	              </tr>
	              <tr>
	                <td><input type="submit"  id="SubmitButton1" value="Upload" />
	                <br />You can only upload files < 2 MB
	                </td>
	              </tr>
	              <tr>
	                <td>&nbsp;
	                	<input type="hidden" name="option" value="back" />
	                	<input type="hidden" id="userid" name="userid" value="<?php echo $userid; ?>"  />
	                </td>
	              </tr>
	            </table>
	            
	            </form>
	        </div>
	        <div class="tab-pane" id="orange">
	            <form action="<?php echo base_url("ajax/changecolor")?>" method="post" enctype="multipart/form-data" id="changecolor">
				 Background Color :
	            <table width="500" border="0">
	              <tr>
	                <input type="text" id="color" name="color" value="#123456" />
	                	    	</form>
	                 <div id="colorpicker"></div>
	                 <div id="colorsetis" style="width: 40px;height: 30px;">&nbsp;</div>
	                 <button id="setbackcolor">set</button>
	                 <input type="hidden" name="colorset" id="colorset" value="" />
	                <div id="progressbox1"><div id="progressbar1"></div ><div id="statustxt1">0%</div ></div>
	                <div id="output1"></div>
	              </tr>
	              <tr>
	                <td>&nbsp;
	                	<input type="hidden" name="option" value="back" />
	                	<input type="hidden" id="color_handle" name="color1" value="" />
	                	<input type="hidden" id="userid" name="userid" value="<?php echo $userid; ?>"  />
	                </td>
	              </tr>
	            </table>
	            
	            </form>
	        </div>
		</div>
	</div>
	
	<!-- <form class="navbar-search pull-right" method="post" action="../schools/redirect_to_school_page">
	  <input type="text" class="schools search-query span2" placeholder="Search">
	  <button class="btn btn-success btn-primary" style="margin-top: 7px;" type="submit">Go!</button>
	</form> -->
	
	<div class="container-fluid middle">
		<div class="row-fluid">