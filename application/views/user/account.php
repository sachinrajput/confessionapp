
<h1>Edit User</h1>
<p>Please enter the users information below.</p>

<?php if($message != "" ){ ?>
    <div class="alert" id="infoMessage">
    	<button type="button" class="close" data-dismiss="alert">&times;</button>
    	<?php echo $message; ?> 
    </div>
<?php } ?>

<?php echo form_open(current_url()); ?>

<p>
    First Name: <br />
    <?php echo form_input($first_name); ?>
</p>

<p>
    Last Name: <br />
    <?php echo form_input($last_name); ?>
</p>

<p>
    Company Name: <br />
    <?php echo form_input($company); ?>
</p>

<p>
    Password: (if changing password)<br />
    <?php echo form_input($password); ?>
</p>

<p>
    Confirm Password: (if changing password)<br />
    <?php echo form_input($password_confirm); ?>
</p>

<?php echo form_hidden('id', $user->id); ?>
<?php echo form_hidden($csrf); ?>

<p><?php echo form_submit('submit', 'Save'); ?></p>

<?php echo form_close(); ?>
