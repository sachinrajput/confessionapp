<?php 
	if ($this->ion_auth->logged_in())
	{
		$this->load->view('includes/header_main.php');
	}
	else{
		$this->load->view('includes/header_login.php');
	}
?>
<?php
	$this->load->view($message_element);
?>
<?php 
	if ($this->ion_auth->logged_in())
	{
		$this->load->view('includes/footer_main.php');
	}
	else{
		$this->load->view('includes/footer_login.php');
	}
?>