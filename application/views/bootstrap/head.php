<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Confession App</title>
  <meta name="description" content="manage your contacts with name, email and phone">
  <meta name="author" content="confession">
  <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  <!-- Le styles -->
  <link href="<?=base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/bootstrap-responsive.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/docs.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/landing.css'); ?>" rel="stylesheet" type="text/css">
  <link href="<?=base_url('assets/css/secure-home.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Le fav and touch icons -->
  <link href="<?=base_url('assets/ico/favicon.ico'); ?>" rel="shortcut icon">
</head>
<body>