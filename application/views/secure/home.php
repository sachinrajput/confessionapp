<?php 
	if($this->ion_auth->logged_in()){
		$userid = $this->session->userdata('user_id');
		$username = $this->session->userdata('username');
	}
	
	
	//var_dump($this->session->all_userdata());
?>
        
            <div class="span3">
                <div class="well sidebar-nav ">
                	<ul class="thumbnails">
                	  <li class="span11">
                	    <div class="thumbnail">
                	      <img data-src="holder.js/300x200" alt="300x200" style="width: 300px; height: 200px;" src="<?php echo base_url('assets/img/no-image.gif');?>">
                	      <div class="caption">
	                          <h3>Thumbnail label</h3>
                	      </div>
                	    </div>
                	  </li>
                	</ul>
                	<ul class="nav nav-list">
                      <li class="nav-header">Select: </li>
                      <li id="postconf"><a href="#" id="postconfession">Confess</a></li>
                      <li id="postsec"><a href="#" id="postsecret">Secret</a></li>
                      <li id="postcr"><a href="#" id="postcrush">Crush</a></li>
                    </ul>
                </div><!--/.well -->
            </div><!--/span-->
            
            <div class="span9">
                <div class="hero-unit">
                    <div>
                        <span id="confess">Connecting...</span>
                        <input type="text" id="confess_i" disabled="disabled" />
                        
                        <span id="secret">Connecting...</span>
                        <input type="text" id="secret_i" disabled="disabled" />
                        
                        <span id="crush">Connecting...</span>
                        <input type="text" id="crush_i" disabled="disabled" />
                        
                        <input type="hidden" id="userid" value="<?php echo $userid; ?>"  />
                        <input type="hidden" id="username" value="<?php echo $username; ?>"  />
                        
                        <input type="hidden" id="datetime"  value="<?php echo date("Y-m-d H:i:s");?>" />
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="well well-large">
                      Live Feeds:
                      <div id="content"></div>
                      
                      
                    </div>
                </div><!--/row-->
                <div class="row-fluid">
                
                </div><!--/row-->
            </div><!--/span-->
       

       