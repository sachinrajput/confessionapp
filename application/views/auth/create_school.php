
<h1>Create School</h1>
<p>Please enter the school information below.</p>

<div id="infoMessage"><?php //echo $message;?></div>
<?php 

	/* $url = 'http://blog.suny.edu/wp-content/uploads/2013/03/Binghamton-Baxter-the-Bearcat-Photo.jpg';
	$img = 'assets/img/schools/binghy.jpg';
	file_put_contents($img, file_get_contents($url));
	*/
?>
<?php echo form_open("auth/create_school_post");?>

      <p>
            School Name: <br />
            <?php echo form_input($school_name);?>
      </p>

      <p>
            School URL: <br />
            <?php echo form_input($url_code);?>
      </p>

      <p>
            Logo Link: <br />
            <?php echo form_input($logo);?>
      </p>
      
      <p>
            School Domain: <br />
            <?php echo form_input($domain);?>
      </p>


      <p><?php echo form_submit('submit', 'Create School');?></p>

<?php echo form_close();?>
