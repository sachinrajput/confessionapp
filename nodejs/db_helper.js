var mysql = require('mysql');
var MYSQL_USERNAME = 'samperk1_confess';
var MYSQL_PASSWORD = 'confession123';

// init;
var client = mysql.createConnection({
  user: MYSQL_USERNAME,
  password: MYSQL_PASSWORD,
});

/* // destroy old db
client.query('DROP DATABASE IF EXISTS samperk1_nodesql', function(err) {
  if (err) { throw err; }
});

// create database
client.query('CREATE DATABASE samperk1_nodesql', function(err) {
  if (err) { throw err; }
}); 
console.log('database samperk1_nodesql is created.'); */
client.query('USE samperk1_confession');
client.query('SET time_zone = "-5:00"');


/* // create table
var sql = ""+
"create table employees("+
" id int unsigned not null auto_increment,"+
" name varchar(50) not null default 'unknown',"+
" salary dec(10,2) not null default 100000.00,"+
" primary key (id)"+
");";
client.query(sql, function(err) {
  if (err) { throw err; }
});
console.log('table employees is created.');
*/



// function to add comment to post
exports.post_comment = function(data, callback) {
 client.query("insert into comments (userpostid, commenter_userid,comment, datetime) values (?,?,?,?)", [data.userpostid, data.userid, data.comment,data.datetime], function(err, info) {
 	if(err){
 		console.log("Data recieved: "+ data.userid+' '+data.comment+' '+ data.userpostid);
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('Message '+data.userid+' - '+data.comment+' saved in db.'); 
  });
}

// function to add add_like to post
exports.add_like = function(data, callback) {
 client.query("insert into likes (type, userpostid,count_likes,userid) values (?,?,0,?)", [data.type, data.userpostid,data.userid], function(err, info) {
 	if(err){
 		console.log("Data recieved: "+ data.userpostid+' '+data.type);
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('Like '+data.userid+' - '+data.comment+' saved in db.'); 
  });
}


// function to add add_like to post
exports.remove_like = function(data,callback) {
 client.query("delete from likes where userpostid = ? and userid = ? ", [data.userpostid,data.userid],function(err, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('like deleted in db.'); 
  });
}

// function to add add_like to post
exports.add_likes_count = function(data,callback) {
 client.query("insert into likes_count (type, contentid,count_likes) values (?,?,0)", [data.type,data.userpostid], function(err, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('like added in db.'); 
  });
}

// function to add add_like to post
exports.update_like = function(data,callback) {
 client.query("update likes_count set count_likes = count_likes + 1 where contentid = ? and type = ?",[data.userpostid,data.type], function(err,results, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	client.query("select count_likes from likes_count where contentid = ? and type = ?",[data.userpostid,data.type], function(err,results, info) {
 	
    	// callback function returns last insert id
    	callback(results);
    	console.log('like incremented in db.'); 
  	});
  });
}

// function to add add_like to post
exports.delete_like = function(data,callback) {
 client.query("update likes_count set count_likes = count_likes - 1 where contentid = ? and type = ?",[data.userpostid,data.type], function(err,results, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	client.query("select count_likes from likes_count where contentid = ? and type = ?",[data.userpostid,data.type], function(err,results, info) {
 	
 		// callback function returns last insert id
 		callback(results);
 		console.log('like reduced from db.'); 
 		});
  });
}


// function to add add_like to post
exports.add_spam = function(data, callback) {
 client.query("insert into spam_report (type,postid,count,userid) values (?,?,0,?)", [data.type, data.userpostid,data.userid], function(err, info) {
 	if(err){
 		console.log("Data recieved: "+ data.userpostid+' '+data.type);
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('Spam '+data.userid+' - '+data.comment+' saved in db.'); 
  });
}


// function to add add_like to post
exports.remove_spam = function(data,callback) {
 client.query("delete from spam_report where postid = ? and userid = ? ", [data.userpostid,data.userid],function(err, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('spam deleted in db.'); 
  });
}

// function to add add_like to post
exports.add_spams_count = function(data,callback) {
 client.query("insert into spam_report_count (postid,count) values (?,0)", [data.userpostid], function(err, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('spam added in db.'); 
  });
}

// function to add add_like to post
exports.update_spam = function(data,callback) {
 client.query("update spam_report_count set count = count + 1 where postid = ? ",[data.userpostid], function(err,results, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	client.query("select count from spam_report_count where postid = ? ",[data.userpostid], function(err,results, info) {
 	
    	// callback function returns last insert id
    	callback(results);
    	console.log('spam incremented in db.'); 
  	});
  });
}

// function to add add_like to post
exports.delete_spam = function(data,callback) {
 client.query("update spam_report_count set count = count - 1 where postid = ? ",[data.userpostid,data.type], function(err,results, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	client.query("select count from spam_report_count where postid = ? ",[data.userpostid], function(err,results, info) {
 	
 		// callback function returns last insert id
 		callback(results);
 		console.log('spam reduced from db.'); 
 		});
  });
}





// function to get list of posts
exports.get_comment = function(commentid,callback) {
  client.query("SELECT c.id,username, CONCAT(first_name,' ' ,last_name) as fullname, userpostid,commenter_userid, comment,DATE_FORMAT(c.datetime,'%Y-%m-%d %H:%i:%s') as datetime,IFNULL(thumbname,'assets/img/no-image.gif') as thumbname FROM  comments as c join users as u on commenter_userid = u.id left join user_profileimage as ui on ui.userid = c.commenter_userid where c.id = ? ",[commentid], function(err, results, fields) {
  	if(err){
  		console.log("Data recieved: "+commentid);
  		console.log("Data recieved: Error on query: "+ err);
  	}
    // callback function returns employees array
    callback(results);
  });
}

// function to get list of posts
exports.get_comments = function(data,callback) {
  if (data.optionid == 0) {
  	client.query("SELECT c.id as id,username, CONCAT(first_name,' ' ,last_name) as fullname ,userpostid,commenter_userid, comment,DATE_FORMAT(c.datetime,'%Y-%m-%d %H:%i:%s') as datetime,IFNULL(thumbname,'assets/img/no-image.gif') as thumbname FROM  comments as c join users as us on commenter_userid = us.id join users_posts as u on u.id = c.userpostid  left join user_profileimage as ui on ui.userid = c.commenter_userid where school_id = ? order by c.id asc",[data.schoolid], function(err, results, fields) {
  		if(err){
  			console.log("Data recieved: "+ data.schoolid+' '+data.optionid);
  			console.log("Data recieved: Error on query: "+ err);
  		}
  	  // callback function returns employees array
  	  callback(results);
  	});
  } else {
  	client.query("SELECT c.id as id,username,userpostid,commenter_userid, comment,DATE_FORMAT(c.datetime,'%Y-%m-%d %H:%i:%s') as datetime,IFNULL(thumbname,'assets/img/no-image.gif') as thumbname FROM  comments as c join users as us on commenter_userid = us.id join users_posts as u on u.id = c.userpostid left join user_profileimage as ui on ui.userid = c.commenter_userid where school_id = ? and post_type = ? ",[data.schoolid,data.optionid], function(err, results, fields) {
  		if(err){
  			console.log("Data recieved: "+ data.schoolid+' '+data.optionid);
  			console.log("Data recieved: Error on query: "+ err);
  		}
  	  // callback function returns employees array
  	  callback(results);
  	});
  }
  
}



// function to get list of posts
exports.get_likes = function(data,callback) {
  client.query("SELECT u.id as userpostid,school_id, post_type, status,IFNULL(l.id,0) as likes,l.userid FROM  users_posts AS u JOIN  users AS us ON u.userid = us.id left join `likes` as l on l.userpostid = u.id  where school_id = ? and l.userid = ? ORDER BY datetime DESC",[data.schoolid,data.userid], function(err, results, fields) {
  	if(err){
  		console.log("Data recieved: "+ data.schoolid+' '+data.userid);
  		console.log("Data recieved: Error on query: "+ err);
  	}
    // callback function returns employees array
    callback(results);
  });
}

// function to get list of posts
exports.get_spams = function(data,callback) {
  client.query("SELECT u.id as postid,school_id, post_type, status,IFNULL(s.id,0) as spams,s.userid FROM  users_posts AS u JOIN  users AS us ON u.userid = us.id left join `spam_report` as s on s.postid = u.id  where school_id = ? and s.userid = ? ORDER BY datetime DESC",[data.schoolid,data.userid], function(err, results, fields) {
  	if(err){
  		console.log("Data recieved: "+ data.schoolid+' '+data.userid);
  		console.log("Data recieved: Error on query: "+ err);
  	}
    // callback function returns employees array
    callback(results);
  });
}

// function to post 
exports.add_message = function(data, callback) {
 client.query("insert into users_posts (userid, message, datetime, school_id,post_type,status,post_no) values (?,?,?,?,?,1,?)", [data.userid, data.msg, data.time,data.schoolid,data.optionid,data.postno], function(err, info) {
 	if(err){
 		console.log("Data recieved: "+ data.userid+' '+data.msg+' '+ data.time);
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
    // callback function returns last insert id
    callback(info.insertId);
    console.log('Message '+data.userid+' - '+data.msg+' saved in db.'); 
  });
}

// function to add add_like to post
exports.increase_count_post = function(data,callback) {
 client.query("select * from user_post_count where schoolid = ? and post_type = ?",[data.schoolid,data.optionid], function(err, results, fields) {
 	if(err){
 		console.log("Data recieved: "+ data.schoolid+' '+data.optionid);
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	
 	if(results.length > 0){
 		//console.log(results[0].count); lets update the count
 		client.query("update user_post_count set count = count + 1 where schoolid = ? and post_type = ?",[data.schoolid,data.optionid], function(err,results, info) {
 			if(err){
 				console.log("Data recieved: Error on query: "+ err);
 			}
 			client.query("select count from user_post_count where schoolid = ? and post_type = ?",[data.schoolid,data.optionid], function(err,results, info) {
 			
 		   	// callback function returns last insert id
 		   	callback(results[0].count);
 		   	console.log('like saved in db.'); 
 		 	});
 		 });
 	}
 	else {
 		//lets insert this new entry first
 		client.query("insert into user_post_count (schoolid, post_type, count) values (?,?,0)", [data.schoolid,data.optionid], function(err, info) {
 			if(err){
 				console.log("Data recieved: "+ data.userid+' '+data.msg+' '+ data.time);
 				console.log("Data recieved: Error on query: "+ err);
 			}
 			client.query("update user_post_count set count = count + 1 where schoolid = ? and post_type = ?",[data.schoolid,data.optionid], function(err,results, info) {
 				if(err){
 					console.log("Data recieved: Error on query: "+ err);
 				}
 				client.query("select count from user_post_count where schoolid = ? and post_type = ?",[data.schoolid,data.optionid], function(err,results, info) {
 				
 			   	// callback function returns last insert id
 			   	callback(results[0].count);
 			   	console.log('like saved in db.'); 
 			 	});
 			 });
 		 });
 	}	
   // callback function returns employees array
   //callback(results);
 });
 
 /* client.query("insert into user_post_count (schoolid, post_type,count) values (?,?,0)",[data.userpostid,data.type], function(err,results, info) {
 	if(err){
 		console.log("Data recieved: Error on query: "+ err);
 	}
 	client.query("select count_likes from likes_count where contentid = ? and type = ?",[data.userpostid,data.type], function(err,results, info) {
 	
    	// callback function returns last insert id
    	callback(results);
    	console.log('like saved in db.'); 
  	});
  }); */
}


// function to get list of posts
exports.get_messages = function(data,callback) {
  if (data.optionid == 0) {
	  	client.query("select * from messages_list  where school_id = ? ORDER BY userpostid DESC limit 20",[data.schoolid], function(err, results, fields) {
	  		if(err){
	  			console.log("Data recieved: "+ data.schoolid+' '+data.optionid);
	  			console.log("Data recieved: Error on query: "+ err);
	  		}
	  	  // callback function returns employees array
	  	  callback(results);
	  	});
  } else {
  		client.query("select * from messages_list where school_id = ? and post_type = ? ORDER BY userpostid DESC limit 20",[data.schoolid,data.optionid], function(err, results, fields) {
  			if(err){
  				console.log("Data recieved: "+ data.schoolid+' '+data.optionid);
  				console.log("Data recieved: Error on query: "+ err);
  			}
  		  // callback function returns employees array
  		  callback(results);
  		});
  }
}
