
var fs = require('fs');

var db_helper = require("./db_helper.js");

var cluster = require('cluster');
var http = require('http');
var numCPUs = require('os').cpus().length;

var webSocketServer = require('websocket').server;
var webSocketsServerPort = 1026;


if (cluster.isMaster) {
  // Fork workers.
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', function(worker, code, signal) {
    console.log('worker ' + worker.process.pid + ' died');
  });
} else {
  // Workers can share any TCP connection
  // In this case its a HTTP server
  /**
   * HTTP server
   */
  var server = http.createServer(function(request, response) {
      // Not important for us. We're writing WebSocket server, not HTTP server
  });
  
  
  server.listen(webSocketsServerPort, function() {
      console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
  });
  
  /**
   * WebSocket server
   */
  var wsServer = new webSocketServer({
      // WebSocket server is tied to a HTTP server. WebSocket request is just
      // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
      httpServer: server
  });
}