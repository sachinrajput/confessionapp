var fs = require('fs');
var cluster = require('cluster');
var numCPUs = 10;//require('os').cpus().length;




/* new */

if (cluster.isMaster) {
    // this is the master control process
    console.log("Control process running: PID=" + process.pid);

    // fork as many times as we have CPUs
    //var numCPUs = require("os").cpus().length;

    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }

    // handle unwanted worker exits
    cluster.on("exit", function(worker, code) {
        if (code != 0) {
            console.log("Worker crashed! Spawning a replacement.");
            cluster.fork();
        }
    });

    // I'm using the SIGUSR2 signal to listen for reload requests
    // you could, instead, use file watcher logic, or anything else
    process.on("SIGUSR2", function() {
        console.log("SIGUSR2 received, reloading workers");

        // delete the cached module, so we can reload the app
        delete require.cache[require.resolve("./app")];

        // only reload one worker at a time
        // otherwise, we'll have a time when no request handlers are running
        var i = 0;
        var workers = Object.keys(cluster.workers);
        var f = function() {
            if (i == workers.length) return; 

            console.log("Killing " + workers[i]);

            cluster.workers[workers[i]].disconnect();
            cluster.workers[workers[i]].on("disconnect", function() {
                console.log("Shutdown complete");
            });
            var newWorker = cluster.fork();
            newWorker.on("listening", function() {
                console.log("Replacement worker online.");
                i++;
                f();
            });
        }
        f();
    });
} else {
    //var app = require("./app");
    
    var db_helper = require("./db_helper.js");
    
    // http://ejohn.org/blog/ecmascript-5-strict-mode-json-and-more/
    "use strict";
    
    // Optional. You will see this name in eg. 'ps' or 'top' command
    process.title = 'confession-app';
    
    // Port where we'll run the websocket server
    var webSocketsServerPort = 1028;
    
    // websocket and http servers
    var webSocketServer = require('websocket').server;
    var http = require('http');
    
    var wsServer;
    var server;
    
    server = http.createServer(function(request, response) {
        // Not important for us. We're writing WebSocket server, not HTTP server
    });
    
    server.listen(webSocketsServerPort, function() {
        console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
    });
    
    wsServer = new webSocketServer({
        // WebSocket server is tied to a HTTP server. WebSocket request is just
        // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
        httpServer: server
    });
    
    /**
     * Global variables
     */
    // latest 100 messages
    
    // list of currently connected clients (users)
    var clients = [ ];
    
    /**
     * Helper function for escaping input strings
     */
    function htmlEntities(str) {
        return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;')
                          .replace(/>/g, '&gt;').replace(/"/g, '&quot;');
    }
    
    // This callback function is called every time someone
    // tries to connect to the WebSocket server
    wsServer.on('request', function(request) {
        console.log((new Date()) + ' Connection from origin ' + request.origin + '.');
    	
        // accept connection - you should check 'request.origin' to make sure that
        // client is connecting from your website
        // (http://en.wikipedia.org/wiki/Same_origin_policy)
        var connection = request.accept(null, request.origin); 
        // we need to know client index to remove them on 'close' event
        var index = clients.push(connection) - 1;
        var userName = false;
        var userColor = false;
    
        console.log((new Date()) + ' Connection accepted.');
    	console.log((new Date()) + ' Requesting School ID....');
    	
    	connection.sendUTF(JSON.stringify( { type: 'getschoolid' } ));
    	
    	
        // send back chat history
        /* if (history.length > 0) {
            connection.sendUTF(JSON.stringify( { type: 'history', data: history} ));
        } */
    	
        // user sent some message
        connection.on('message', function(array_data) {
        
            var type = array_data.type;
        	//console.log(array_data);
        	//var data_rcvd = JSON.stringify(array_data.utf8Data);
        	var data_rcvd = JSON.parse(array_data.utf8Data);
        	var requesttype = data_rcvd.requesttype;
        	
        	if (type === 'utf8') { //accept only text
        	if(requesttype === 'schoolpage'){
        		var schoolid = data_rcvd.schoolid;
        		var optionid = data_rcvd.optionid;
        		var userid = data_rcvd.userid;
        		
        		console.log((new Date()) + ' Recieved School ID:'+ schoolid);
        		console.log((new Date()) + ' Recieved Feature ID:'+ optionid);
        		console.log((new Date()) + ' Recieved User ID:'+ userid);
        		
        		var school_det = {
        			schoolid : schoolid,
        			optionid : optionid,
        			userid : userid
        		};
        		
        		//populate the page with posts
        		db_helper.get_messages(school_det,function(posts) {
        		  connection.sendUTF(JSON.stringify( { type: 'populate', data: posts} ));
        		});
        		
        		//populate the posts with comments
        		db_helper.get_comments(school_det,function(comments) {
        		  connection.sendUTF(JSON.stringify( { type: 'populate_comments', data: comments} ));
        		});
        		
        		//populate the posts with comments
        		db_helper.get_likes(school_det,function(likes) {
        		  connection.sendUTF(JSON.stringify( { type: 'populate_likes', data: likes} ));
        		});
        		
        		//populate the posts with comments
        		db_helper.get_spams(school_det,function(spams) {
        		  connection.sendUTF(JSON.stringify( { type: 'populate_spams', data: spams} ));
        		});
        		
        		
        	} else if(requesttype === 'add_like'){
        		var userpostid = data_rcvd.userpostid;
        		var type = data_rcvd.type;
        		var userid = data_rcvd.userid;
        		var post_type = data_rcvd.post_type;
        		//console.log((new Date()) + ' Recieved like ID:'+ post_type);
        		var like_det = {
        			userpostid : userpostid,
        			type : type,
        			userid: userid
        		};
        		
        		    		
        		//populate the posts with like
        		db_helper.add_like(like_det,function(likeid) {
        		  
        		  var like_det1 = {
        		  	userpostid : userpostid,
        		  	type : type,
        		  	userid: userid,
        		  	like_id: likeid
        		  };
        		  
        		  //Also add a like entry for this in likes table 
        		  db_helper.update_like(like_det1, function(likeid1) {
        		  	console.log(likeid1[0]);
        		  	//var like_count = JSON.parse(likeid1[0]);
        		  	//console.log(like_count);
        		  	console.log((new Date()) + ' Recieved l1ike ID:'+ post_type);
        		  	var like_det2 = {
        		  		userpostid : userpostid,
        		  		type : type,
        		  		userid: userid,
        		  		likeid: likeid,
        		  		count_likes: likeid1[0],
        		  		post_type : post_type
        		  	};
        		  	
        		  	connection.sendUTF(JSON.stringify( { type: 'add_like', data: like_det2} ));
        		  	
        		  });
        		  
        		  
        		});
        		
        		
        	} else if(requesttype === 'remove_like'){
        		var userpostid = data_rcvd.userpostid;
        		var type = data_rcvd.type;
        		var userid = data_rcvd.userid;
        		var post_type = data_rcvd.post_type;
        		
        		var like_det = {
        			userpostid : userpostid,
        			type : type,
        			userid: userid
        		};
        		
        		    		
        		//populate the posts with like
        		db_helper.remove_like(like_det,function(likeid) {
        		  
        		  var like_det1 = {
        		  	userpostid : userpostid,
        		  	type : type,
        		  	userid: userid,
        		  	likeid: likeid
        		  };
        		  
        		  //Also add a like entry for this in likes table 
        		  db_helper.delete_like(like_det1, function(likeid1) {
        		  	console.log(likeid1[0]);
        		  	//var like_count = JSON.parse(likeid1[0]);
        		  	//console.log(like_count);
        		  	var like_det2 = {
        		  		userpostid : userpostid,
        		  		type : type,
        		  		userid: userid,
        		  		likeid: likeid,
        		  		count_likes: likeid1[0],
        		  		post_type: post_type
        		  	};
        		  	
        		  	connection.sendUTF(JSON.stringify( { type: 'remove_like', data: like_det2} ));
        		  	
        		  });
        		  
        		  
        		});
        		
        		
        	} else if(requesttype === 'mark_spam'){
        		var userpostid = data_rcvd.userpostid;
        		var type = data_rcvd.type;
        		var userid = data_rcvd.userid;
        		var post_type = data_rcvd.post_type;
        		//console.log((new Date()) + ' Recieved like ID:'+ post_type);
        		var spam_det = {
        			userpostid : userpostid,
        			type : type,
        			userid: userid
        		};
        		
        		    		
        		//populate the posts with like
        		db_helper.add_spam(spam_det,function(spamid) {
        		  
        		  var spam_det1 = {
        		  	userpostid : userpostid,
        		  	type : type,
        		  	userid: userid,
        		  	spam_id: spamid
        		  };
        		  
        		  //Also add a like entry for this in likes table 
        		  db_helper.update_spam(spam_det1, function(spamid1) {
        		  	console.log(spamid1[0]);
        		  	//var like_count = JSON.parse(likeid1[0]);
        		  	//console.log(like_count);
        		  	console.log((new Date()) + ' Recieved spam1 ID:'+ post_type);
        		  	var spam_det2 = {
        		  		userpostid : userpostid,
        		  		type : type,
        		  		userid: userid,
        		  		spamid: spamid,
        		  		count: spamid1[0],
        		  		post_type : post_type
        		  	};
        		  	
        		  	connection.sendUTF(JSON.stringify( { type: 'mark_spam', data: spam_det2} ));
        		  	
        		  });
        		  
        		  
        		});
        		
        		
        	} else if(requesttype === 'unmark_spam'){
        		var userpostid = data_rcvd.userpostid;
        		var type = data_rcvd.type;
        		var userid = data_rcvd.userid;
        		var post_type = data_rcvd.post_type;
        		
        		var spam_det = {
        			userpostid : userpostid,
        			type : type,
        			userid: userid
        		};
        		
        		    		
        		//populate the posts with like
        		db_helper.remove_spam(spam_det,function(spamid) {
        		  
        		  var spam_det1 = {
        		  	userpostid : userpostid,
        		  	type : type,
        		  	userid: userid,
        		  	spamid: spamid
        		  };
        		  
        		  //Also add a like entry for this in likes table 
        		  db_helper.delete_spam(spam_det1, function(spamid1) {
        		  	console.log(spamid1[0]);
        		  	//var like_count = JSON.parse(likeid1[0]);
        		  	//console.log(like_count);
        		  	var spam_det2 = {
        		  		userpostid : userpostid,
        		  		type : type,
        		  		userid: userid,
        		  		spamid: spamid,
        		  		count: spamid1[0],
        		  		post_type: post_type
        		  	};
        		  	
        		  	connection.sendUTF(JSON.stringify( { type: 'unmark_spam', data: spam_det2} ));
        		  	
        		  });
        		  
        		  
        		});
        		
        		
        	} else if(requesttype === 'comment'){
        		var schoolid = data_rcvd.schoolid;
        		var optionid = data_rcvd.optionid;
        		var userid = data_rcvd.userid;
        		var comment = data_rcvd.comment;
        		var postid = data_rcvd.postid;
        		var datetime = data_rcvd.datetime;
        		
        		
        		console.log((new Date()) + ' Recieved Comment :'+ comment);
        		console.log((new Date()) + ' Recieved Feature ID:'+ optionid);
        		
        		
        		var comment_det = {
        			schoolid : schoolid,
        			optionid : optionid,
        			userid : userid,
        			comment : comment,
        			userpostid : postid,
        			datetime: datetime,
        			type : 'comment'
        		};
        		
        		//populate the posts with their comments
        		db_helper.post_comment(comment_det,function(lastId) {
        		  console.log('Saved to db!' + lastId);
        		 
        		  //Also add a like entry for this in likes table 
        		  /* db_helper.add_likes_count(comment_det, function(likeid) {
        		  	
        		  }); */
        		  
        		  db_helper.get_comment(lastId,function(comment_details) {
        		    
        		    // broadcast message to all connected clients
        		    //var comment_details = JSON.parse(comment_details.utf8Data);
        		    
        		    console.log(comment_details[0]);
        		    
        		    //	connection.sendUTF(JSON.stringify( { type: 'populate', data: posts} ));
        		    var json = JSON.stringify({ type:'add_new_comments', data: comment_details[0] });
        		    
        		    for (var i=0; i < clients.length; i++) {
        		        clients[i].sendUTF(json);
        		    }
        		  });
        		  
        		});
        		
        		
        	} else if(requesttype === 'post'){
        	
    	    	var message = data_rcvd.msg;
    	    	var userid = data_rcvd.userid;
    	    	var username = data_rcvd.username;
    	    	var datetime = data_rcvd.datetime;
    	    	var schoolid = data_rcvd.schoolid;
    	    	var optionid = data_rcvd.optionid;
    	    	
    	    	// create emitters for messages rcvd
    	    	/* db_helper.get_messages(function(messages) {
    	    	  io.sockets.emit('populate', messages);
    	    	}); */
    	    	
    	    	console.log(data_rcvd);  
    	    		
    	    	userName = username;
        	
    	        
    	                console.log((new Date()) + ' Received Message from '
    	                            + userName + ': ' + message);
    	                
    	                  
    	                var data = {
    	                  userid: userid,
    	                  msg: message,
    	                  schoolid : schoolid,
    	                  optionid : optionid,
    	                  time: datetime,
    	                  username: userName,
    	                  optionid: optionid
    	                };
    	                
    	              //Increase the count flag for this post wrt school id and post type
    	              db_helper.increase_count_post(data, function(post_no) {
    	              
    	              		var data2 = {
    	              		  userid: userid,
    	              		  msg: message,
    	              		  schoolid : schoolid,
    	              		  optionid : optionid,
    	              		  time: datetime,
    	              		  username: userName,
    	              		  optionid: optionid,
    	              		  postno : post_no
    	              		};
    					  // create post, when its done repopulate employees on client
    					  db_helper.add_message(data2, function(lastId) {
    					   	console.log('Saved to db!' + lastId);
    					   					   	
    					   	var data1 = {
    					   	  userid: userid,
    					   	  msg: message,
    					   	  schoolid : schoolid,
    					   	  optionid : optionid,
    					   	  time: datetime,
    					   	  username: userName,
    					   	  userpostid: lastId,
    					   	  type : 'post',
    					   	  comment_count : 0,
    					   	  postno : post_no
    					   	};
    					   	
    					   	
    					   	
    					   	//Also add a like entry for this in likes table 
    					   	db_helper.add_likes_count(data1, function(likeid) {
    					   		
    					   	});
    					   	
    					   	//Also add a spam entry for this in likes table 
    					   	db_helper.add_spams_count(data1, function(likeid) {
    					   		
    					   	});
    					   	
    					   	
    					   	// broadcast message to all connected clients
    					   	var json = JSON.stringify({ type:'message', data: data1 });
    					   	
    					   	for (var i=0; i < clients.length; i++) {
    					   	    clients[i].sendUTF(json);
    					   	}
    					   	
    					   	
    					  });
    					  
    				  });
    				  //alert(userpostid);
    				  
    					
    					
    	                
    	         }
            	}
            
        });
    
        // user disconnected
        connection.on('close', function(connection) {
            if (userName !== false && userColor !== false) {
                console.log((new Date()) + " Peer "
                    + connection.remoteAddress + " disconnected.");
                // remove user from the list of connected clients
                clients.splice(index, 1);
                
            }
        });
    
    });
    
    
}

/**
 * HTTP server
 */
/* var server = http.createServer(function(request, response) {
    // Not important for us. We're writing WebSocket server, not HTTP server
});


server.listen(webSocketsServerPort, function() {
    console.log((new Date()) + " Server is listening on port " + webSocketsServerPort);
});

/**
 * WebSocket server
 */
/* var wsServer = new webSocketServer({
    // WebSocket server is tied to a HTTP server. WebSocket request is just
    // an enhanced HTTP request. For more info http://tools.ietf.org/html/rfc6455#page-6
    httpServer: server
}); */


