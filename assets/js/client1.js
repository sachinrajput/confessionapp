$(function () {
    "use strict";

    // for better performance - to avoid searching in DOM
    var content = $('#content');
    
    var input = $('#confess_i');
    var input1 = $('#secret_i');
    var input2 = $('#crush_i');
    
    var status = $('#confess');
    var status1 = $('#secret');
    var status2 = $('#crush');
    
    var userid = $('#userid');
    
    var username = $('#username'); 
    
    var schoolid = $('#schoolid');
    var optionid = $('#option');
    var comment_button = $('#comment_button');

	var confession_option = $('#postconfession');
	var secret_option = $('#postsecret');
	var crush_option = $('#postcrush');
	var all_option = $('#all_posts');
	var options_select = $('#option_selected');
	
    // my color assigned by the server 
    var myColor = false;
    // my name sent to the server
    var myName = false;
	var datetime = $('#datetime');
	
    // if user is running mozilla then use it's built-in WebSocket
    window.WebSocket = window.WebSocket || window.MozWebSocket;

    // if browser doesn't support WebSocket, just show some notification and exit
    if (!window.WebSocket) {
        content.html($('<p>', { text: 'Sorry, but your browser doesn\'t '
                                    + 'support WebSockets.'} ));
        input.hide();
        input1.hide();
        input2.hide();
        
        $('span').hide();
        return; 
    }

    // open connection 
    var connection = new WebSocket('ws://50.87.27.245:1028');

    connection.onopen = function (client) {
    
        // first we want users to enter their names
        input.removeAttr('disabled');
        input1.removeAttr('disabled');
        input2.removeAttr('disabled');
        
        status.text('');
        status1.text('');
        status2.text('');  
              
        
        
    };

    connection.onerror = function (error) {
        // just in there were some problems with conenction...
        content.html($('<p>', { text: 'Sorry, but there\'s some problem with your '
                                    + 'connection or the server is down.' } ));
    };

    // most important part - incoming messages
    connection.onmessage = function (message) {
        // try to parse JSON message. Because we know that the server always returns
        // JSON this should work without any problem but we should make sure that
        // the massage is not chunked or otherwise damaged.
        try {
            var json = JSON.parse(message.data);
        } catch (e) {
            console.log('This doesn\'t look like a valid JSON: ', message.data);
            return;
        }
		
		//datetime = new Date(json.data[i].time);
		
        // NOTE: if you're not sure about the JSON structure
        // check the server source code above
        if(json.type === 'getschoolid'){
        	
        	connection.send(JSON.stringify({
        		requesttype : 'schoolpage',
        		schoolid : schoolid.val(),
        		optionid : optionid.val(),
        		userid : userid.val()
        	}));        	
        	
        } else if (json.type === 'populate_comments') { // entire message history
        	/**
        	 * Append all Comments
        	 */
            // insert every single message to the chat window
            //alert("fg");
            for (var i=0; i < json.data.length; i++) {
                CommentList(json.data[i].comment, json.data[i].datetime,json.data[i].id,json.data[i].userpostid,json.data[i].thumbname,json.data[i].fullname);
            }
        } else if (json.type === 'populate_likes') { // entire message history
        	/**
        	 * Append all likes
        	 */
            // insert every single message to the chat window
            //alert("fg");
            for (var i=0; i < json.data.length; i++) {
                if(json.data[i].likes != 0){
                	appendLikes(json.data[i].userpostid);
                }
            }            
        } else if (json.type === 'add_new_comments') { // entire message history
        	/**
        	 * Add new Comments
        	 */
            // insert new comment below the post
            //input.removeAttr('disabled'); // let the user write another message
            //alert(json.data.thumbname);
            
            addNewComment(json.data.comment, json.data.datetime,json.data.id,json.data.userpostid,json.data.thumbname,json.data.fullname);
        } else if (json.type === 'add_like') { // entire message history
        	/**
        	 * Add single like 
        	 */
            // insert new comment below the post
            //input.removeAttr('disabled'); // let the user write another message
            //alert(json.data.post_type);
            if(json.data.type === 'comment')
            	addNewLike_to_comment(json.data.userpostid, json.data.likeid,json.data.userid,json.data.type);
            else
            	addNewLike_to_post(json.data.userpostid, json.data.likeid,json.data.userid,json.data.type,json.data.count_likes.count_likes,json.data.post_type);
        } else if (json.type === 'remove_like') { // entire message history
        	/**
        	 * Remove single like 
        	 */
            // insert new comment below the post
            //input.removeAttr('disabled'); // let the user write another message
            //alert(json.data.id);
            if(json.data.type === 'comment')
            	removeLike_from_comment(json.data.userpostid, json.data.likeid,json.data.userid,json.data.type);
            else
            	removeLike_from_post(json.data.userpostid, json.data.likeid,json.data.userid,json.data.type,json.data.count_likes.count_likes,json.data.post_type);
        } else {
            console.log('Hmm..., I\'ve never seen JSON like this: ', json);
        }
    };
 
    /**
     * Send mesage when user presses Enter key
     */
     
     
    input.keydown(function(e) {
    	if (event.keyCode == 13 && event.shiftKey) {
    	     var content = this.value;
    	     var caret = getCaret(this);
    	     this.value = content.substring(0,caret)+"\n"+content.substring(carent,content.length-1);
    	     event.stopPropagation();
    	     
    	} else if(event.keyCode == 13)
    	{
    	    var msg = $(this).val();
            var user_id = userid.val();
            var user_name = username.val();
            var dt = datetime.val();
            var options_select1 = options_select.val();
            //alert(options_select1);
            //alert(dt);
            if (!msg) {
                return;
            }
            // send the message as an ordinary text
            connection.send(JSON.stringify({
              requesttype : 'post',
              userid: user_id,
              username: user_name,
              datetime: dt,
              msg: msg,
              schoolid : schoolid.val(),
              optionid : options_select1
            }));
            
            //connection.send(userid,msg);
            $(this).val('');
            // disable the input field to make the user wait until server
            // sends back response
            input.attr('disabled', 'disabled');

            // we know that the first message sent from a user their name
            if (myName === false) {
                myName = user_name;
            }
        }
    });

	

	confession_option.click(function (e) {
		var user_id = userid.val();
		content.empty();
		connection.send(JSON.stringify({
			requesttype : 'schoolpage',
			schoolid : schoolid.val(),
			optionid : optionid.val(),
			userid : user_id
		}));
	});
	
	secret_option.click(function (e) {
		var user_id = userid.val();
		content.empty();
		connection.send(JSON.stringify({
			requesttype : 'schoolpage',
			schoolid : schoolid.val(),
			optionid : optionid.val(),
			userid : user_id
		}));
	});
	
	crush_option.click(function (e) {
		var user_id = userid.val();
		content.empty();
		connection.send(JSON.stringify({
			requesttype : 'schoolpage',
			schoolid : schoolid.val(),
			optionid : optionid.val(),
			userid : user_id
		}));
	});
	
	all_option.click(function (e) {
		var user_id = userid.val();
		content.empty();
		connection.send(JSON.stringify({
			requesttype : 'schoolpage',
			schoolid : schoolid.val(),
			optionid : optionid.val(),
			userid : user_id 
		}));
	});
	
	
	
	
	/*$('#content').on('load','.comment', function () {
		$(media).children('.comment').css('display','none');
	}); */
	
	
	$('#content').on('click', 'div.media > div.media-body > div.user_post > div.btn-group > button#comment_button', function() {
	  //alert($(this).attr('id'));
	  if (!$(this).attr('data-toggled') || $(this).attr('data-toggled') == 'off'){
	  	  /* currently it's not been toggled, or it's been toggled to the 'off' state,
	  	             so now toggle to the 'on' state: */
	  	  $(this).attr('data-toggled','on');
	  	  var media = $(this).parent('div').parent('div').parent('div').parent('div');
	  	  var commid = media.attr('id').split('_');
	  	  var comment_count = $(media).children('.comment_count_class').attr('id');
	  	  //alert(comment_count);
	  	  $(media).children('.comment').css('display','block');
	  	  if(comment_count != '0')
	  	  $(media).children('.comment_list').css('display','block');
	  } else if ($(this).attr('data-toggled') == 'on'){
		  /* currently it has been toggled, and toggled to the 'on' state,
		             so now turn off: */
		  $(this).attr('data-toggled','off');
		  var media = $(this).parent('div').parent('div').parent('div').parent('div');
		  var commid = media.attr('id').split('_');
		  
		  //alert(commid);
		  $(media).children('.comment').css('display','none');
		  $(media).children('.comment_list').css('display','none');
	  }
	}
	); 
	
	
	$('#content').on('keydown', 'div.media > div.comment > textarea.comment_text', function(event) {
	  	if (event.keyCode == 13 && event.shiftKey) {
	           var content = this.value;
	           var caret = getCaret(this);
	           this.value = content.substring(0,caret)+"\n"+content.substring(carent,content.length-1);
	           event.stopPropagation();
	           
	      } else if(event.keyCode == 13)
	      {
	      	  var comm = $(this).parent('div');
	      	  var commid = $('> .userpost_id', comm);
	      	  
	          var comment = $(this).val();
	          var postid = commid.attr("value");
	          var dt = datetime.val();
	          /*
	          	Let's send the comment to database
	          */	          
	          connection.send(JSON.stringify({
	          	requesttype : 'comment',
	          	schoolid : schoolid.val(),
	          	optionid : optionid.val(),
	          	userid : userid.val(),
	          	datetime: dt,
	          	comment : comment,
	          	postid : postid
	          }));
	          
	          
	          //alert('Comment is : '+ comment + ' id: '+commid.attr("value"));
	          //alert('Comment Submitted!');
	          $(this).val('');
	      }
	  
	}); 
	
	$('#content').on('click', 'div.media > div.media-body > div.user_post > div.btn-group > button#like_button', function() {
		//add to db about this like with the userid and postid 
		//do display none for this and display inline block for other 
		//alert($(this).val());
		//alert(userid.val());
		var userpostid = $(this).val();
		var type = 'post';
		var user_id = userid.val();
		var media = $(this).parent('div').parent('div').parent('div').parent('div');
		
		var post_type = $(media).children('.post_type_class').attr('id');
		//var post_type = optionid.val();
		//alert(post_type);
		//Let below things be done by server call
		//$(this).addClass("liked");
		//$(this).removeAttr("id");
		
		/*
			Let's send the like to database
		*/	          
		connection.send(JSON.stringify({
			requesttype : 'add_like',
			userpostid : userpostid,
			type: type,
			userid : user_id,
			post_type: post_type
		}));
		
	});
	
	
	$('#content').on('click', 'div.media > div.media-body > div.user_post > div.btn-group > button.liked', function() {
		//add to db about this like with the userid and postid 
		//do display none for this and display inline block for other 
		//alert($(this).val());
		
		var userpostid = $(this).val();
		var type = 'post';
		var user_id = userid.val();
		var media = $(this).parent('div').parent('div').parent('div').parent('div');
		
		var post_type = $(media).children('.post_type_class').attr('id');
		//alert(post_type);
		// let this be done server
		//$(this).removeClass("liked");
		//$(this).attr("id","like_button");
		
		/*
			Let's reomve the like from database
		*/	          
		connection.send(JSON.stringify({
			requesttype : 'remove_like',
			userpostid : userpostid,
			type: type,
			userid : user_id,
			post_type : post_type
		}));
	});
	
	
	
	
    /**
     * This method is optional. If the server wasn't able to respond to the
     * in 3 seconds then show some error message to notify the user that
     * something is wrong.
     */
    setInterval(function() {
        if (connection.readyState !== 1) {
            status.text('Error');
            input.attr('disabled', 'disabled').val('Sachin - Chat server closed!Unable to comminucate '
                                                 + 'with the WebSocket server.');
        }
    }, 3000);



	function getCaret(el) { 
	  if (el.selectionStart) { 
	    return el.selectionStart; 
	  } else if (document.selection) { 
	    el.focus(); 
	
	    var r = document.selection.createRange(); 
	    if (r == null) { 
	      return 0; 
	    } 
	
	    var re = el.createTextRange(), 
	        rc = re.duplicate(); 
	    re.moveToBookmark(r.getBookmark()); 
	    rc.setEndPoint('EndToStart', re); 
	
	    return rc.text.length; 
	  }  
	  return 0; 
	}
	
	
    /**
     * Add message to the chat window
     */
     
     /* 
     	<h5 class="pull-left">'+author+'</h5><a class="pull-left" href="#"><img class="media-object" data-src="holder.js/64x64" src="http://samperkstech.com/confession/assets/img/no-image.gif" style="width: 64px; height: 64px;"></a>
     
     */
     
    function addMessage(author, message, dt,id,post_type,comment_count,post_no) {
    	//alert(post_type);
    	
    	var posttype = '';
    	var likes = '';
    	switch (post_type) {
    		case '1':
    			posttype = '<span class="label label-black pull-right">Confession#'+post_no+'</span>';
    			likes = '<span class="badge badge-inverse">0</span>';
    			break;
    		case '2':
    			posttype = '<span class="label label-warning pull-right">Secret#'+post_no+'</span>';
    			likes = '<span class="badge badge-warning">0</span>';
    			break;
    		case '3':
    			posttype = '<span class="label label-red pull-right">Crush#'+post_no+'</span>';
    			likes = '<span class="badge badge-important">0</span>';
    			break;
    	}
    	//alert(posttype);
        content.prepend('<div class="media user_post_main" id="userpost_'+id+'"><div class="media-body">'+posttype+'<h5 class="media-heading">'+message+'</h5><div class="pull-right" id="like_'+id+'" style="vertical-align:top;">'+likes+'</div><div class="user_post pull-right"><div class="btn-group btn-group-horizontal"><button type="button" class="btn" id="like_button" value="'+id+'" title="Like"><i class="icon-thumbs-up"></i></button><button type="button" class="btn" id="comment_button" value="'+id+'" title="Comment"><i class="icon-edit"></i></button><button type="button" class="btn" title="Share"><i class="icon-share"></i></button></div></div></div><div class="comment" id="comment'+id+'" style="display:none"><textarea type="text" placeholder="Press Enter to Comment and Shift + Enter for new line" name="comment" class="comment_text" id="comment_'+id+'" /><input type="hidden" class="userpost_id" name="userpostid" value="'+id+'" /> </div><div class="comment_list well" id="commentlist_'+id+'" style="display:none"></div><p><h6 class="text-info pull-right"><i class="icon-time"></i><abbr class="timeago" title="'+dt+'"></abbr></h6></p><div class="post_type_class" id="'+post_type+'"></div><div class="comment_count_class" id="'+comment_count+'"></div></div>');
        
        var tag = ' .media > .text-info > .timeago';
        //alert(tag);
        $(tag).timeago();
        switch (post_type) {
        	case 1:
        		$('[title]').colorTip({color:'black'});
        		break;
        	case 2:
        		$('[title]').colorTip({color:'yellow'});
        		break;
        	case 3:
        		$('[title]').colorTip({color:'red'});
        		break;
        }
    }
     
    /**
     * Add message to the chat window
     */
    function addMessage1(author, message, dt,id,count_likes,post_type,comment_count,i,post_no) {
    	
    	
        //.timeago();
    	var posttype = '';
    	var likes = '';
    	switch (post_type) {
    		case 1:
    			posttype = '<span class="label label-black pull-right">Confession#'+post_no+'</span>';
    			likes = '<span class="badge badge-inverse">'+count_likes+'</span>';
    			break;
    		case 2:
    			posttype = '<span class="label label-warning pull-right">Secret#'+post_no+'</span>';
    			likes = '<span class="badge badge-warning">'+count_likes+'</span>';
    			break;
    		case 3:
    			posttype = '<span class="label label-red pull-right">Crush#'+post_no+'</span>';
    			likes = '<span class="badge badge-important">'+count_likes+'</span>';
    			break;
    	}
    	
        content.append('<div class="media user_post_main" id="userpost_'+id+'"><div class="media-body">'+posttype+'<h5 class="media-heading">'+message+'</h5><div class="pull-right" id="like_'+id+'" style="vertical-align:top;">'+likes+'</div><div class="user_post pull-right"><div class="btn-group btn-group-horizontal"><button type="button" class="btn" id="like_button" value="'+id+'" title="Like"><i class="icon-thumbs-up"></i></button><button type="button" class="btn" id="comment_button" value="'+id+'" title="Comment"><i class="icon-edit"></i></button><button type="button" class="btn" title="Share"><i class="icon-share"></i></button></div></div></div><div class="comment" id="comment'+id+'" style="display:none"><textarea type="text" placeholder="Press Enter to Comment and Shift + Enter for new line" name="comment" class="comment_text" id="comment_'+id+'" /><input type="hidden" class="userpost_id" name="userpostid" value="'+id+'" /></div><div class="comment_list well" id="commentlist_'+id+'" style="display:none"></div><p><h6 class="text-info pull-right"><i class="icon-time"></i><abbr class="timeago" title="'+dt+'"></abbr></h6></p><div class="post_type_class" id="'+post_type+'"></div><div class="comment_count_class" id="'+comment_count+'"></div></div>');
        
        
        	var tag = ' .media > .text-info > .timeago';
        	//alert(tag);
        	$(tag).timeago();
        	switch (post_type) {
        		case 1:
        			$('[title]').colorTip({color:'black'});
        			break;
        		case 2:
        			$('[title]').colorTip({color:'yellow'});
        			break;
        		case 3:
        			$('[title]').colorTip({color:'red'});
        			break;
        	}
        	
        
    }
    
    
    /* 
    	Add new comment 
    */
    
    function addNewComment(comment, time,commentid,postid,thumbname,fullname) {
    	//create handler for this comment post with respect to the postid and commentid
    	
    	var tag = '#userpost_'+postid+' > #commentlist_'+postid;
    	//alert(tag);
    	$('#userpost_'+postid).children('.comment_list').css('display','block');
    	$('#commentlist_'+postid).slideDown(10000,function(){
    		$(tag).prepend('<div class="media comm_list" ><div class="media-body"><div class="pull-left"><a class="pull-left" href="#"><img class="media-object" data-src="holder.js/64x64" src="../'+thumbname+'" style="width: 32px; height: 32;"></a></div><b>'+fullname+'</b> '+comment+'<p class="pull-right"><i class="icon-time"></i><abbr class="timeago" title="'+time+'"></abbr></p></div></div>');
    		//$('[title]').colorTip({color:'black'});
    	});
    	
    	var tag1 = ' .media > .comment_list > .comm_list > .media-body > .pull-right > .timeago';
    	//alert(tag);
    	$(tag1).timeago();
    	
    	var tag2 = '#userpost_'+postid+' > .comment_count_class';
    	$(tag2).attr('id',1);
    	//$(tag).slideDown('slow', function() {
    	    // Animation complete.
    	    
    	  //});
    	
    	//$(tag).slideDown("slow");
    	/*
    		<div class="media" style="margin-top: 25px;"><div class="media-body"><h5 class="media-heading">Media heading</h5>comments here .......</div><p class="pull-right">time....</p></div>
    	*/
    	
    }
    
    
    /* 
    	Append comment list 
    */
    
    function CommentList(comment, time,commentid,postid,thumbname,fullname) {
    	//create handler for this comment post with respect to the postid and commentid
    	
    	var tag = '#userpost_'+postid+' > #commentlist_'+postid;
    	//alert(thumbname);
    	$(tag).prepend('<div class="media comm_list" ><div class="media-body"><div class="pull-left"><a class="pull-left" href="#"><img class="media-object" data-src="holder.js/64x64" src="../'+thumbname+'" style="width: 32px; height: 32px;"></a></div><b>'+fullname+'</b> '+comment+'<p class="pull-right"><i class="icon-time"></i><abbr class="timeago" title="'+time+'"></abbr></p></div></div>');
    	
    	var tag1 = ' .media > .comment_list > .comm_list > .media-body > .pull-right > .timeago';
    	//alert(tag);
    	$(tag1).timeago();
    	//$('[title]').colorTip({color:'black'});
    }    
    
    function addNewLike_to_post(userpostid,likeid, userid, type,count_likes,post_type) {
    
      var tag = '#userpost_'+userpostid+' > .media-body > .user_post > .btn-group > #like_button';
      var tag1 = '#userpost_'+userpostid+' > .media-body > #like_'+userpostid;
      //alert(post_type);
      
      var likes = '';
      switch (post_type) {
      	case '1':
      		
      		likes = '<span class="badge badge-inverse">'+count_likes+'</span>';
      		break;
      	case '2':
      		
      		likes = '<span class="badge badge-warning">'+count_likes+'</span>';
      		break;
      	case '3':
      		
      		likes = '<span class="badge badge-important">'+count_likes+'</span>';
      		break;
      }
      //alert(likes);
      $(tag1).html(likes);
      $(tag).addClass("liked");
      $(tag).removeAttr("id");
    }
    
    function removeLike_from_post(userpostid,likeid, userid, type,count_likes,post_type) {
    
      var tag = '#userpost_'+userpostid+' > .media-body > .user_post > .btn-group > .liked';
      var tag1 = '#userpost_'+userpostid+' > .media-body > #like_'+userpostid;
      //alert(post_type);
      
      var likes = '';
      switch (post_type) {
      	case '1':
      		
      		likes = '<span class="badge badge-inverse">'+count_likes+'</span>';
      		break;
      	case '2':
      		
      		likes = '<span class="badge badge-warning">'+count_likes+'</span>';
      		break;
      	case '3':
      		
      		likes = '<span class="badge badge-important">'+count_likes+'</span>';
      		break;
      }
      $(tag1).html(likes);
      $(tag).attr('id','like_button');
      $(tag).removeClass("liked");
    }
    
    function appendLikes(userpostid) {
    
      var tag = '#userpost_'+userpostid+' > .media-body > .user_post > .btn-group > #like_button';
      
      $(tag).addClass("liked");
      $(tag).removeAttr("id");
    }
    
    function get_readable_time(dt) {
    	//2013-08-04 23:56:31
    	
    }
    
    function getISODateTime(dt){
        // padding function
        //var s = function(a,b){return(1e15+a+"").slice(-b)};
    
        // default date parameter
        if (typeof dt === 'undefined'){
            dt = new Date();
        };
        // return ISO datetime
        return dt.getFullYear() + '-' +
            (dt.getMonth() + 1 < 10 ? '0' + dt.getMonth() + 1  : dt.getMonth() + 1 ) + '-' +
            (dt.getDate() < 10 ? '0' + dt.getDate() : dt.getDate()) + ' ' +
            (dt.getHours() < 10 ? '0' + dt.getHours() : dt.getHours()) + ':' +
            (dt.getMinutes() < 10 ? '0' + dt.getMinutes() : dt.getMinutes()) + ':' +
            (dt.getSeconds() < 10 ? '0' + dt.getSeconds() : dt.getSeconds());
    }
    
    function _format_date(unix_timestamp) {
      var difference_in_seconds = (Math.round((new Date()).getTime() / 1000)) - unix_timestamp,
          current_date = new Date(unix_timestamp * 1000), minutes, hours,
          months = new Array(
            'January','February','March','April','May',
            'June','July','August','September','October',
            'November','December');
      
      if(difference_in_seconds < 60) {                                  
        return difference_in_seconds + " second" + _plural(difference_in_seconds) + " ago";
      } else if (difference_in_seconds < 60*60) {
        minutes = Math.floor(difference_in_seconds/60);
        return minutes + " minute" + _plural(minutes) + " ago";
      } else if (difference_in_seconds < 60*60*24) {
        hours = Math.floor(difference_in_seconds/60/60);
        return hours + " hour" + _plural(hours) + " ago";
      } else if (difference_in_seconds > 60*60*24){
        if(current_date.getYear() !== new Date().getYear()) 
          return current_date.getDay() + " " + months[current_date.getMonth()].substr(0,3) + " " + _fourdigits(current_date.getYear());
        
        return current_date.getDay() + " " + months[current_date.getMonth()].substr(0,3);
      }
      
      return difference_in_seconds;
      
      function _fourdigits(number)	{
            return (number < 1000) ? number + 1900 : number;}
    
      function _plural(number) {
        if(parseInt(number) === 1) {
          return "";
        }
        return "s";
      }
    }
});
